import { useState, useCallback } from "react";
import { shouldShowFeature } from "../utils";

export const useFeatureFlag = (flag: string): [Boolean, Function] => {
  const [showFeature, setValue] = useState<Boolean>(shouldShowFeature(flag));
  const setShowFeature = useCallback((value) => setValue(value), []);
  return [showFeature, setShowFeature];
};

export default useFeatureFlag;