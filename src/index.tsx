import FeatureFlag from "./featureFlag";

export { useFeatureFlag } from "./useFeatureFlag";

export default FeatureFlag;