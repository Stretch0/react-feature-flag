export const isNullOrUndefined = (input: any): Boolean => input === null || input === undefined;

export const getUrlParameter = (name = ""): null|boolean =>  {
  name = name.replace(/[[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(window ? window.location.search : '');
  return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' ')) === "true";
};

export const getCookie = (name: string): null|boolean => {
  const value = `; ${document && document.cookie}`;
  const parts: any = value.split(`; ${name}=`);
  parts

  if (parts.length !== 2) {
    return null
  }

  return parts
    .pop()
    .split(";")
    .shift() === "true"
}

export const shouldShowFeature = (flag: string): Boolean => {
  
  const envVar = process.env[flag] === "true";
  const queryString = getUrlParameter(flag);
  const cookie = getCookie(flag);

  const queryStringOverride = !isNullOrUndefined(queryString);
  const cookieOverride = !isNullOrUndefined(cookie);

  let showFeature: Boolean|null|undefined = envVar;

  if(cookieOverride){
    showFeature = cookie;
  }
  
  if(queryStringOverride){
    showFeature = queryString;
  }
  
  return !!showFeature;
  
}