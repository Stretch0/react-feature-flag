import { FunctionComponent, ReactNode, ReactElement } from "react";
import PropTypes from "prop-types";
import { shouldShowFeature } from "../utils";

interface IFeatureFlag {
  flag: string;
  children: ReactNode;
}

const FeatureFlag: FunctionComponent<IFeatureFlag> = ({
  flag,
  children
}: IFeatureFlag) => {
  const showFeature = shouldShowFeature(flag);
  return showFeature ? children as ReactElement<any> : null;
};

FeatureFlag.propTypes = {
  flag: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
}

export default FeatureFlag;