import { renderHook, act } from '@testing-library/react-hooks';
import { useFeatureFlag } from "../useFeatureFlag";

describe("useFeatureFlag", () => {
  it('should be defined', () => {
    expect(useFeatureFlag).toBeDefined();
  });

  it('renders the hook correctly and checks types', () => {
    const { result } = renderHook(() => useFeatureFlag("ENABLE_FLAG"));
    expect(result.current[0]).toEqual(false);
    expect(result.current[1]).toEqual(expect.any(Function))
  });

  it('should toggle flag value', () => {
    const { result } = renderHook(() => useFeatureFlag("ENABLE_FLAG"));
    act(() => {
      result.current[1](true);
    });
    expect(result.current[0]).toBe(true);
    act(() => {
      result.current[1](false);
    });
    expect(result.current[0]).toBe(false);
  });
})