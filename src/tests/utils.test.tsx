import {
  isNullOrUndefined,
  getUrlParameter,
  getCookie,
  shouldShowFeature
} from "../utils";

describe("isNullOrUndefined", () => {
  it("Should return true when value is null", () => {
    expect(isNullOrUndefined(null)).toEqual(true);
  })
  it("Should return true when value is undefined", () => {
    expect(isNullOrUndefined(undefined)).toEqual(true);
  })
  it("Should return false when value is false", () => {
    expect(isNullOrUndefined(false)).toEqual(false);
  })
  it("Should return true when value is false", () => {
    expect(isNullOrUndefined(true)).toEqual(false);
  })
})

describe("getUrlParameter", () => {
  it("Should return 'true'", () => {
    Object.defineProperty(window, 'location', {
      writable: true,
      value: {
        search: "?QUERY_1=true"
      }
    });
    expect(getUrlParameter("QUERY_1")).toEqual(true);
  })
  it("Should return 'false'", () => {
    Object.defineProperty(window, 'location', {
      writable: true,
      value: {
        search: "?QUERY_2=false"
      }
    });
    expect(getUrlParameter("QUERY_2")).toEqual(false);
  })
  it("Should return null when no query is present", () => {
    Object.defineProperty(window, 'location', {
      writable: true,
      value: {
        search: ""
      }
    });
    expect(getUrlParameter("QUERY_3")).toEqual(null);
  })
})

describe("getCookie", () => {
  it("Should return true", () => {
    Object.defineProperty(document, 'cookie', {
      writable: true,
      value: "ENABLE_FEATURE=true"
    });
    expect(getCookie("ENABLE_FEATURE")).toEqual(true)
  })
  it("Should return false", () => {
    Object.defineProperty(document, 'cookie', {
      writable: true,
      value: "ENABLE_FEATURE=false"
    });
    expect(getCookie("ENABLE_FEATURE")).toEqual(false)
  })
  it("Should return null when no cookie is set", () => {
    Object.defineProperty(document, 'cookie', {
      writable: true,
      value: ""
    });
    expect(getCookie("ENABLE_FEATURE")).toEqual(null)
  })
  it("Should return null when no cookie is found", () => {
    Object.defineProperty(document, 'cookie', {
      writable: true,
      value: "SOME_OTHER_COOKIE=true"
    });
    expect(getCookie("ENABLE_FEATURE")).toEqual(null)
  })
})

describe("shouldShowFeature", () => {
  
  afterEach(() => {
    Object.defineProperty(process, 'env', {
      writable: true,
      value: {
        ENABLE_FEATURE: undefined
      }
    });
    Object.defineProperty(document, 'cookie', {
      writable: true,
      value: undefined
    });
    Object.defineProperty(window, 'location', {
      writable: true,
      value: {
        search: ""
      }
    });
  })
  
  describe("env var === true", () => {
    
    it("Should return true when only env var is set", () => {
      Object.defineProperty(process, 'env', {
        writable: true,
        value: {
          ENABLE_FEATURE: "true"
        }
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(true)
    })
    it("Should return false when env var is set to true but cookie is false", () => {
      Object.defineProperty(process, 'env', {
        writable: true,
        value: {
          ENABLE_FEATURE: "true"
        }
      });
      Object.defineProperty(document, 'cookie', {
        writable: true,
        value: "ENABLE_FEATURE=false"
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(false)
    })
    it("Should return false when env var is set to true but query string is false", () => {
      Object.defineProperty(process, 'env', {
        writable: true,
        value: {
          ENABLE_FEATURE: "true"
        }
      });
      Object.defineProperty(window, 'location', {
        writable: true,
        value: {
          search: "?ENABLE_FEATURE=false"
        }
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(false)
    })
  })

  describe("env var === false", () => {
    it("Should return false when only env var is set", () => {
      Object.defineProperty(process, 'env', {
        writable: true,
        value: {
          ENABLE_FEATURE: "false"
        }
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(false)
    })
    it("Should return true when env var is set to false but cookie is true", () => {
      Object.defineProperty(process, 'env', {
        writable: true,
        value: {
          ENABLE_FEATURE: "false"
        }
      });
      Object.defineProperty(document, 'cookie', {
        writable: true,
        value: "ENABLE_FEATURE=true"
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(true)
    })
    it("Should return false when env var is set to false but query string is true", () => {
      Object.defineProperty(process, 'env', {
        writable: true,
        value: {
          ENABLE_FEATURE: "false"
        }
      });
      Object.defineProperty(window, 'location', {
        writable: true,
        value: {
          search: "?ENABLE_FEATURE=true"
        }
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(true)
    })
  })

  describe("cookie", () => {
    it("Should return true when only cookie is true", () => {
      Object.defineProperty(document, 'cookie', {
        writable: true,
        value: "ENABLE_FEATURE=true"
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(true)
    })
    it("Should return true when cookie is false but query string is true", () => {
      Object.defineProperty(document, 'cookie', {
        writable: true,
        value: "ENABLE_FEATURE=false"
      });
      Object.defineProperty(window, 'location', {
        writable: true,
        value: {
          search: "?ENABLE_FEATURE=true"
        }
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(true)
    })
  })

  describe("query string", () => {
    it("Should return true when only query string is true", () => {
      Object.defineProperty(window, 'location', {
        writable: true,
        value: {
          search: "?ENABLE_FEATURE=true"
        }
      });
      expect(shouldShowFeature("ENABLE_FEATURE")).toEqual(true)
    })
  })
})