import React from "react";
import { create } from 'react-test-renderer';
import FeatureFlag from "../featureFlag";

describe("FeatureFlag", () => {
  it('should be defined', () => {
    expect(FeatureFlag).toBeDefined();
  });

  it('renders children when env var is set to true', () => {
    const testRenderer = create(
      <FeatureFlag flag="REACT_APP_TRUE_FLAG">
        <div>Hello</div>
      </FeatureFlag>
    );
    expect(testRenderer.root.children).toHaveLength(1)
  });

  it('doesn\'t render children when env var is set to false', () => {
    const testRenderer = create(
      <FeatureFlag flag="REACT_APP_FALSE_FLAG">
        <div>Hello</div>
      </FeatureFlag>
    );
    expect(testRenderer.root.children).toHaveLength(0)
  });

})