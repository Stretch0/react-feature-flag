# React Feature Flag

A comprehensive feature flag package for React

## How to use
Feature flags can be toggled in order to hide or show new features in your react app. These can be toggled in 1 of 4 ways: (In order of highest priority)

 - React hook
 - URL query string parameter
 - Browser cookie 
 - environment variable

### Higher order component
```jsx
import FeatureFlag from "react-feature-flag";

const MyComponent = () => {
  return (
    <FeatureFlag flag="REACT_APP_ENABLE_FEATURE">
      <div>My Feature</div>
    </FeatureFlag>
  )
}
```

### React hook
```jsx
import { useEffect } from "react";
import { useFeatureFlag } from "react-feature-flag";

const [showFeature, setShowFeature] = useFeatureFlag("REACT_APP_ENABLE_FEATURE")

useEffect(() => {
  setShowFeature(true)
}, [])  

return showFeature && <div>Hello</div>;
```